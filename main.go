package main

import (
	"flag"
	"fmt"
	linuxproc "github.com/c9s/goprocinfo/linux"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"
)

// docker build -t run-when-cpu-busy .
// docker run -it run-when-cpu-busy -percent 70 -interval 1s -simulate 3 bash -c 'echo threshold breached $DELTA $PERCENT $INTERVAL'

const JiffiesPerSecond = 100

var percent = flag.Float64("percent", 80, "percentage threshold, run script when CPU is above this level")
var interval = flag.Duration("interval", 10*time.Second, "interval between samples")
var stress = flag.Uint64("stress", 0, "for simulation purposes, number of stress tasks to run on the system, each consuming one CPU")

func sampleCPUCountersWeighted() (float64, error) {
	stat, err := linuxproc.ReadStat("/proc/stat")
	if err != nil {
		return 0, err
	}

	utilization := stat.CPUStatAll.User + stat.CPUStatAll.System
	weighted := float64(utilization) / float64(JiffiesPerSecond) / float64(len(stat.CPUStats))

	return weighted, nil
}

func main() {
	flag.Parse()

	args := flag.Args()
	if len(args) == 0 {
		log.Fatalf("missing command")
	}

	if _, err := os.Stat("/proc/stat"); os.IsNotExist(err) {
		log.Fatalf("/proc/stat does not exist, are you running linux?")
	}

	if *percent <= 0 || *percent > 100 {
		log.Fatalf("--percent must be between 0 and 100")
	}

	if *stress > 0 {
		fmt.Println("simulate", stress)
		fmt.Println("numcpu", runtime.NumCPU())
		for i := uint64(0); i < *stress; i++ {
			go func() {
				j := 0
				for {
					j += 1
				}
				// unreachable
				fmt.Println(j)
			}()
		}
	}

	start, err := sampleCPUCountersWeighted()
	if err != nil {
		log.Fatalf("%v", err)
	}

	for {
		// rate limiting: check if we have reached a rate limit (e.g. 1 execution per hour),
		// sleep until rate limit expires

		time.Sleep(*interval)

		end, err := sampleCPUCountersWeighted()
		if err != nil {
			log.Fatalf("%v", err)
		}

		delta := ((end - start) / interval.Seconds()) * 100
		if delta > *percent {
			cmd := exec.Command(args[0], args[1:]...)
			cmd.Stdout = os.Stdin
			cmd.Stderr = os.Stderr
			cmd.Env = append(cmd.Env,
				fmt.Sprintf("%s=%.3f", "PERCENT", *percent),
				fmt.Sprintf("%s=%.3f", "DELTA", delta),
				fmt.Sprintf("%s=%s", "INTERVAL", *interval))
			err := cmd.Run()
			if err != nil {
				// TODO: fail gracefully and retry instead of crashing
				log.Fatalf("%v", err)
			}
		}

		start = end
	}
}
